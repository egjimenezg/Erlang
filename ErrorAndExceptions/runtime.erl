%Bad Match Error
[X,Y] = {4,5}.

%Bad argument errors
erlang:binary_to_list("heh, already a list").

%Undefined Function Errors
lists:random([1,2,4]).

%Bad Arithmetic Errors
5+llama.

%Bad Arity Errors
F = fun() -> ok end.
F(a,b).

% System Limit Errors
% http://erlang.org/doc/efficiency_guide/advanced.html

