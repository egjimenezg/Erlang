-module(recursion).
-export([duplicate/2,tail_duplicate/2,reverse/1,tail_reverse/1,
         sublist/2,tail_sublist/2,zip/2,tail_zip/2,
         quicksort/1,partition/4,lc_quicksort/1,
         combinatory/2,
         combinatory/3,
         combine/2,
         concat/2,
         loopList/2]).

duplicate(0,_) -> [];
duplicate(N,Term) when N > 0 -> [Term|duplicate(N-1,Term)].

tail_duplicate(N,Term) ->
  tail_duplicate(N,Term,[]).

tail_duplicate(0,_,List) ->
  List;
tail_duplicate(N,Term,List) when N > 0 ->
  tail_duplicate(N-1,[Term|List]).

reverse([]) -> [];
reverse([H|T]) -> reverse(T)++[H].

tail_reverse(L) -> tail_reverse(L,[]).

tail_reverse([],Acc) -> Acc;
tail_reverse([H|T],Acc) -> tail_reverse(T,[H|Acc]).

sublist(_,0) -> [];
sublist([],_) -> [];
sublist([H|T],N) when N > 0 -> [H|sublist(T,N-1)].

tail_sublist(L,N) -> reverse(tail_sublist(L,N,[])).

tail_sublist(_,0,SubList) -> SubList;
tail_sublist([],_,SubList) -> SubList;
tail_sublist([H|T],N,SubList) when N > 0 -> tail_sublist(T,N-1,[H|SubList]).
% [1,2,3]  [2,3] 2  1[]
% [2,3]    [3]  1  [2]|[1]
% [3]      [1]  0  [3]|[2,1]

zip([],_) -> [];
zip(_,[]) -> [];
zip([X|Xs],[Y|Ys]) -> [{X,Y}|zip(Xs,Ys)].

tail_zip(La,Lb) -> reverse(tail_zip(La,Lb,[])).
tail_zip([],_,Acc) -> Acc;
tail_zip(_,[],Acc) -> Acc;
tail_zip([X|Xs],[Y|Ys],Acc) -> tail_zip(Xs,Ys,[{X,Y}|Acc]).

quicksort([]) -> [];
quicksort([Pivot|Rest]) ->
  { Smaller, Larger } = partition(Pivot,Rest,[],[]),
  quicksort(Smaller) ++ [Pivot] ++ quicksort(Larger).


partition(_,[],Smaller,Larger) -> {Smaller, Larger};
partition(Pivot,[H|T],Smaller,Larger) ->
  if H =< Pivot -> partition(Pivot,T,[H|Smaller],Larger);
     H > Pivot -> partition(Pivot,T,Smaller, [H|Larger])
  end.

lc_quicksort([]) -> [];
lc_quicksort([Pivot|Rest]) ->
  lc_quicksort([Smaller || Smaller <- Rest,Smaller =< Pivot])
  ++ [Pivot] ++
  lc_quicksort([Larger || Larger <- Rest,Larger > Pivot]).

combinatory([],_) ->
  [];
combinatory([H|T],Set) ->
  combinatory(combine(H,Set),Set,tl(Set))++combinatory(T,Set).

combinatory(Items,_,[_]) ->
  Items; 
combinatory(Items,Set,SubSet) ->
 combinatory(loopList(Items,Set),Set,tl(SubSet)).


loopList(_,[]) ->
  [];
loopList([],_) ->
  []; 
loopList([H|T],Set) ->
  combine(H,Set)++loopList(T,Set).

combine(_,[]) ->
  [];
combine(List,[H|T]) ->
  [concat(List,H)|combine(List,T)].

concat([H|T],L) ->
  [H|T]++[L];
concat(Item,L) ->
  [Item,L].
