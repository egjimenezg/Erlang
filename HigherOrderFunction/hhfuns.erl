-module(hhfuns).
-compile(export_all).

one() -> 1.
two() -> 2.

add(X,Y) -> X() + Y().

increment([]) -> [];
increment([H|T]) -> [H+1|increment(T)].

decrement([]) -> [];
decrement([H|T]) -> [H-1|decrement(T)].

map(_,[]) -> [];
map(F,[H|T]) -> [F(H)|map(F,T)].

incr(X) -> X + 1.
decr(X) -> X - 1.

% Anonymous functions
% Fn = fun() -> a end.
% PrepareAlarm = fun(Room) ->
%   io:format("Alarm set in ~s.~n",[Room]),
%   fun() -> io:format("Alarm tripped in ~s! Call Batman!~n",[Room]) end
% end.

base(A) ->
 B = A+1,
 F = fun() -> A * B end,
 F().

a() ->
  Secret = "pony",
  fun() -> Secret end.

b(F) ->
 "a/0's password is "++F().

%baseShadowing(A) ->
%  A = 1,
%  (fun(A) -> A = 2 end)(2).

even(L) -> lists:reverse(even(L,[])).
even([],Acc) -> Acc;
even([H|T],Acc) when H rem 2 == 0 ->
  even(T,[H|Acc]);
even([_|T],Acc) ->
  even(T,Acc).

old_men(L) -> lists:reverse(old_men(L,[])).
old_men([], Acc) -> Acc;
old_men([Person = {male, Age}|People], Acc) when Age > 60 ->
  old_men(People, [Person|Acc]);
old_men([_|People],Acc) ->
  old_men(People,Acc).

filter(Pred,L) -> lists:reverse(filter(Pred,L,[])).
filter(_,[],Acc) -> Acc;
filter(Pred,[H|T],Acc) ->
  case Pred(H) of
    true -> filter(Pred,T,[H|Acc]);
    false -> filter(Pred,T,Acc)
  end.


%Numbers = lists:seq(1,10)
%hhfuns:filter(fun(X) -> X rem 2 == 0 end, Numbers).
%People = [{male,45},{female,67},{male,66},{female,12},{unknown,174},{male,74}].
%hhfuns:filter(fun({Gender,Age}) -> Gender == male andalso Age > 60 end,People).
%

fold(_,Start,[]) -> Start;
fold(F,Start,[H|T]) -> fold(F,F(H,Start),T).

%hhfuns:fold(fun(A,B) when A > B -> B; (_,B) -> B end, H,T).
%hhfuns:fold(fun(A,B) when A < B -> B; (_,B) -> B end, H,T).
%hhfuns:fold(fun(A,B) -> A+B end,0,lists:seq(1,6)).

reverse(L) ->
  fold(fun(X,Acc) -> [X|Acc] end,[],L).

map2(F,L) ->
  reverse(fold(fun(X,Acc) -> [F(X)|Acc] end,[],L)).

filter2(Pred,L) ->
  F = fun(X,Acc) ->
    case Pred(X) of
      true -> [X|Acc];
      false -> Acc
    end
  end,
  reverse(fold(F,[],L)).


