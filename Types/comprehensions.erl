[2*N || N <- [1,2,3,4]].
[X || X <- [1,2,3,4,5,6,7,8,9,10], X rem 2 =:= 0].

RestaurantMenu = [{steak, 5.99}, {beer,3.99}, {poutine,3.50}, {kitten,20.99},{water,0.00}].
[{Item,Price*1.07} || {Item,Price} <- RestaurantMenu, Price >= 3, Price =< 10].

[X+Y || X <- [1,2], Y <- [3,4]].

Weather = [{toronto,rain}, {montreal,storms}, {londong,fog},
           {paris,sun}, {boston,fog},{vancouver,snow}].

FoggyPlaces = [X || {X,fog} <- Weather].
