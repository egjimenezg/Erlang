f().

Point = {4,5}.
{X,Y} = Point.
X.
{X,_} = Point.
{_,_} = {4,5}.
{_,_} = {4,5,6}.


Temperature = 23.213.
PreciseTemperature = {celcius,23.213}.
{kelvin,T} = PreciseTemperature.

{point, {X,Y}}.

[1,2,3,{numbers,[4,5,6]},5.34,atom].
[97,98,99].

[1,2,3] ++ [4,5].
[1,2,3,4,5] -- [1,2,3].
[2,4,2] -- [2,4].
[2,4,2] -- [2,4,2].
[1,2,3] -- [1,2] -- [3].
[1,2,3] -- [1,2] -- [2].

hd([1,2,3,4]).
tl([1,2,3,4]).

List=[2,3,4].
NewList=[1|List].

[Head|Tail] = NewList
Head.
Tail.

[NewHead|NewTail] = Tail.
NewHead.

[1 | []].
[2 | [1 | []]].
[3 | [2 | [1 | []]]].

[a,b,c,d].
[a,b,c,d | []].
[a,b | [c,d]].
[a,b | [c | [d]]].
[a | [b | [c | [d | []]]]].
